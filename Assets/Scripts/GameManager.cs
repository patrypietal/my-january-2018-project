﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private int brickRows = 3;
    private int brickCols = 10;
    private int minBrickRows = 1;
    private int maxBrickRows = 3;
    private int minBrickCols = 5;
    private int maxBrickCols = 10;
    private float ratioAspect;
    private float gameRatioAspect;
    public float gameScreenHeight;
    public float gameScreenWidth;
    private float screenHeight = 10;
    private float screenWidth;
    private float platformWidth;
    private float platformPositionY;
    private float platformHeight = 0.2f;
    private float minPlatformHeight = 0.1f;
    private float maxPlatformHeight = 0.5f;
    private float ScreenStartPositionX;
    private float ScreenStartPositionY = 5;
    private float brickWidth;
    private float brickHeight;
    private float brickDistance;
    public int maxAngle = 45;
    private float maxBrickWidth = 1;
    private float wallWidth = 0.04f;
    private float brickAspectRatio = 0.2f;
    public GameObject brickObject;
    public GameObject wallObject;
    public GameObject plafromObject;
    public GameObject forceWallObject;
    public GameObject ballObject;
    public GameObject plafrom;
    public GameObject ball;
    public List<GameObject> bricksList;
    public List<GameObject> wallsList;
    private float brickToNextBrickWidth;
    private float brickToNextBrickHeight = 0.6f;
    public PlatformMove PlatformMoveScript;
    public BallScript ballScript;
    public PlatformBot platformBotScript = null;

    private IEnumerator coroutine;
    void Start()
    {
        GetGameSettings();
        GetScreenInfo();
        DrawWalls();
        GetBrickPlatformInfo();

        GenerateBricks();
        GenerateBall();//inactive
        GeneratePlatform();
        //active
        MakePlatformBricksAnimation();
    }
    void MakePlatformBricksAnimation()
 {

        coroutine = DisableBallForSeconds(5);
        StartCoroutine(coroutine);
        coroutine = ObjectAnimationEasingPositionY(PlatformMoveScript.gameObject, 3, 0,-gameScreenHeight);
        StartCoroutine(coroutine);
        foreach (GameObject brick in bricksList)
        {
            float randomMaxDelayBeforeBricksAnimation = 0.7f;
            float delay = Random.Range(0f, randomMaxDelayBeforeBricksAnimation);
            coroutine = ObjectAnimationEasingPositionYAndRotateZ(brick, 3, delay, -gameScreenHeight / 3,2);
            StartCoroutine(coroutine);
        }
        //wait 3sec
         

    }

    private void GetGameSettings()
    { 
        if (GameObject.FindGameObjectWithTag("GameSettings") != null)
        {
            GameObject GameSettings = GameObject.FindGameObjectWithTag("GameSettings");

            GameSettingsScript settingScript = GameSettings.GetComponent<GameSettingsScript>();
         brickCols =  Mathf.Clamp( settingScript.platformRows,minBrickCols,maxBrickCols);
             
        }


    }
    public void GameStart()
    {
    }
    private void DrawWalls()
    {
        GameObject wall1 = (GameObject)Instantiate(wallObject, new Vector3(-gameScreenWidth / 2, 0, 1), Quaternion.identity);
        wall1.transform.localScale = new Vector3(wallWidth, gameScreenHeight, 1);
        wallsList.Add(wall1);
        GameObject wall2 = (GameObject)Instantiate(wallObject, new Vector3(gameScreenWidth / 2, 0, 1), Quaternion.identity);
        wall2.transform.localScale = new Vector3(wallWidth, gameScreenHeight, 1);
        wallsList.Add(wall2);
        GameObject wall4 = (GameObject)Instantiate(wallObject, new Vector3(0, gameScreenHeight / 2, 1), Quaternion.identity);
        wall4.transform.localScale = new Vector3(gameScreenWidth, wallWidth, 1);
        wallsList.Add(wall4);
        GameObject wall3 = (GameObject)Instantiate(wallObject, new Vector3(0, -gameScreenHeight/3*2, 1), Quaternion.identity);
        wall3.transform.localScale = new Vector3(gameScreenWidth, wallWidth, 1);
        wallsList.Add(wall3);
        GameObject forceWall1 = (GameObject)Instantiate(forceWallObject, new Vector3(0, (gameScreenHeight/2)-(gameScreenHeight / 10 * 3), 1), Quaternion.identity);
        forceWall1.transform.localScale = new Vector3(gameScreenWidth, wallWidth, 1); 
        GameObject forceWall2 = (GameObject)Instantiate(forceWallObject, new Vector3(0, (gameScreenHeight/2)-(gameScreenHeight / 10 * 8), 1), Quaternion.identity);
        forceWall2.transform.localScale = new Vector3(gameScreenWidth, wallWidth, 1); 

    }
    private void GetScreenInfo()
    {
        gameScreenHeight = screenHeight;
        if (Screen.width ==0 || Screen.height ==0) ratioAspect = 9 / 16;
        else
        {
            float w = Screen.width;
            float h = Screen.height;
            Debug.Log("good width and height");
            Debug.Log("w: " + w);
            Debug.Log("h: " + h);

            ratioAspect = w/h;
        }
        screenWidth = ratioAspect * screenHeight;
        if (screenWidth > 1.5f * brickCols)
        {
            gameScreenWidth = 1.5f * brickCols;
        }
        else gameScreenWidth = screenWidth;
        gameRatioAspect = gameScreenWidth / gameScreenHeight;
        Debug.Log("Screen.width: " + Screen.width);
        Debug.Log("Screen.height: " + Screen.height);
        Debug.Log("screenWidth: " + screenWidth);
        Debug.Log("screenHeight: " + screenHeight);
        Debug.Log("gameScreenWidth: " + gameScreenWidth);
        Debug.Log("gameScreenHeight: " + gameScreenHeight);
        Debug.Log("gameScreenHeight: " + gameScreenHeight);
        ScreenStartPositionX = -gameScreenWidth / 2;

    }
    private void GetBrickPlatformInfo()
    {
        brickWidth = gameScreenWidth / (brickCols * 1.1f + 0.1f); // tyle ile cegiel tyle jest ich szerokosci + tyle jest ich odstępów +1
        brickToNextBrickWidth = brickWidth;
        brickWidth = Mathf.Clamp(brickWidth, maxBrickWidth / 3, maxBrickWidth);
        brickHeight = brickWidth * brickAspectRatio;
        Debug.Log("brickWidth: " + brickWidth);
        brickDistance = gameScreenWidth / 10 / (brickCols + 1); 
        Debug.Log("gameRatioAspect: " + gameRatioAspect);
        platformWidth = 1.1f* gameRatioAspect/(9.0f/16.0f);// szerokosc to 1.1 przy 9:16
        Debug.Log("platformWidth" + platformWidth);
        platformHeight = Mathf.Clamp(platformHeight, minPlatformHeight, maxPlatformHeight);
        platformPositionY = -(gameScreenHeight / 2.0f) + (2.0f * platformHeight);
        Debug.Log("platformPositionY" + platformPositionY);
    }
    
    private bool CheckIfNextPlatformValid(int firstAngle, int secondAngle)
    {
        int angleDifference = Mathf.Abs(firstAngle - secondAngle);
        Debug.Log("firstAngle: " + firstAngle + " secondAngle:" + secondAngle + " angleDifference:" + angleDifference); 
        if (angleDifference > 48) return false;
        if (angleDifference < 38) return true;

        float a1 = Mathf.Tan(Mathf.Deg2Rad * firstAngle);
        float a2 = Mathf.Tan(Mathf.Deg2Rad * secondAngle);

        float x = brickAspectRatio / (a1 - a2);
        float y = x * a1;
        float distance = (x * x) + (y * y);
        Debug.Log("distance: " + distance + " max:" + brickToNextBrickHeight * brickToNextBrickHeight);
        if (distance < Mathf.Pow(1.05f*((brickToNextBrickHeight - brickHeight) / 2), 2)) return false;
        return true;


    }
    private void GenerateBricks()
    {
        for (int col = 1; col <= brickCols; col++)
        {
            int lastAngle = 0;
            int angle = 0;
            for (int row = 1; row <= brickRows; row++)
            {

                bool angleValididate = false;
                while (!angleValididate)
                {
                    angle = Random.Range(-maxAngle, maxAngle);
                       if (Mathf.Abs(angle - lastAngle) < 30)
                    {
                        angle = Random.Range(-maxAngle, maxAngle);

                       }
                    angleValididate = CheckIfNextPlatformValid(angle, lastAngle);

                    }
                GameObject brick = (GameObject)Instantiate(brickObject, new Vector3((col * (brickToNextBrickWidth + brickDistance) - (brickToNextBrickWidth / 2)) + ScreenStartPositionX, ScreenStartPositionY - brickToNextBrickHeight * row + 0.1f, 1), Quaternion.Euler(new Vector3(0, 0, angle)));
                brick.transform.localScale = new Vector3(brickWidth, brickHeight, 1);
                   lastAngle = angle;
                bricksList.Add(brick);

                //brick add to list


            }
        }
    }
    private void ChangePlatformWidth(float width)
    {
        platformWidth = width;
        ChangePlatformWidth();
    }
    private void ChangePlatformWidth()
    {
         
        plafrom.transform.localScale = (new Vector3(platformWidth, platformHeight, 1));

        PlatformMoveScript.platformWidth = platformWidth;
        PlatformMoveScript.UpadePlatformWidth(platformWidth);
        PlatformMoveScript.PlatformPositionUpdate();
    }
    private void GeneratePlatform()
    {
        plafrom = (GameObject)Instantiate(plafromObject, new Vector3(0, -(gameScreenHeight / 2) + (2 * platformHeight), 0), Quaternion.identity); 
        PlatformMoveScript = plafrom.GetComponent<PlatformMove>();
        if (plafrom.GetComponent<PlatformBot>() != null) {
            platformBotScript = plafrom.GetComponent<PlatformBot>();
            platformBotScript.lastBricksRow = ScreenStartPositionY - brickToNextBrickHeight * brickRows;
            platformBotScript.ball = ball;
        } 
        PlatformMoveScript.ballWidth = ball.transform.localScale.x;
        PlatformMoveScript.gameScreenWidth = gameScreenWidth;
        ChangePlatformWidth();
        PlatformBot PlatformBotScript = plafrom.GetComponent<PlatformBot>();
        PlatformBotScript.gameScreenHeight = gameScreenHeight;
            PlatformBotScript.gameScreenWidth = gameScreenWidth;
        plafrom.transform.position = new Vector3(0, platformPositionY, 1);

    }
    private void GenerateBall()
    {
        ball = (GameObject)Instantiate(ballObject, new Vector3(0, -(gameScreenHeight / 2) + (3 * platformHeight)+0.05f, 0), Quaternion.identity);
        ballScript = ball.GetComponent<BallScript>();
        ballScript.gameManagerScript = this;

    }
    public void BackToMainMenu()
    {

    }
    public void PlayAgain()
    {

    }
    void Update()
    {
        if (bricksList.Count == 0)  SceneManager.LoadScene("Loser");
        // dla infinity mode GenerateBricks();
    }
    IEnumerator ObjectAnimationEasingPositionY(GameObject obj, float duration, float delayBeforeStart, float changeInValue)
    {
        float beginningValue = obj.transform.position.y - changeInValue;
        obj.transform.position = new Vector3(obj.transform.position.x, beginningValue, obj.transform.position.z);

        float newYPos;
        if (delayBeforeStart > 0) yield return new WaitForSeconds(delayBeforeStart);
        float currentTime = 0;
        while (currentTime < 1)
        { 
            yield return null;
            currentTime += Time.deltaTime / duration;
            newYPos = EasingFunctions.EaseOutElastic(currentTime, beginningValue, changeInValue, 1);
               obj.transform.position = new Vector3(obj.transform.position.x, newYPos, obj.transform.position.z);
            
            
        }

    }
    IEnumerator ObjectAnimationEasingPositionYAndRotateZ(GameObject obj, float duration, float delayBeforeStart, float changeInValue, float rotationDuration)
    {
        Vector3 objRotation = obj.transform.rotation.eulerAngles;
        float beginningRotation = 0;
        float rotationChangeInValue = objRotation.z;
        objRotation.z = beginningRotation;
        obj.transform.eulerAngles = objRotation;
        float beginningValue = obj.transform.position.y - changeInValue;
        Vector3 objPosition = obj.transform.position;
        objPosition.y = beginningValue;
        obj.transform.position = objPosition;
        float newYPos;
        if (delayBeforeStart > 0) yield return new WaitForSeconds(delayBeforeStart);
        float currentTime = 0;
        while (currentTime < 0.7f)
        { 
            yield return null;
            currentTime += Time.deltaTime / duration;
            newYPos = EasingFunctions.EaseOutElastic(currentTime, beginningValue, changeInValue, 1);
            objPosition.y = newYPos;
            obj.transform.position = objPosition;

        }


        if (rotationChangeInValue > 180) rotationChangeInValue -= 360;
        float newZrotation;
          currentTime = 0;
        while (currentTime < 0.8)
        { 
            yield return null;
            currentTime += Time.deltaTime / rotationDuration;
            newZrotation = EasingFunctions.EaseOutElastic(currentTime, beginningRotation, rotationChangeInValue, 1);
            objRotation.z = Mathf.Clamp (newZrotation,-180f,180f);
            if (float.IsNaN(objRotation.z)) objRotation.z = 0;

            obj.transform.eulerAngles = objRotation;
            }

    }
    IEnumerator DisableBallForSeconds( float Seconds)
    {

        ballScript.gameObject.SetActive(false);
        yield return new WaitForSeconds(Seconds);

        ballScript.gameObject.SetActive(true);

    }
}
