﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonuses : MonoBehaviour { 
    public bool isSlowDown=false;
    public bool isSpeedUp=false;
    public bool isCannonShot=false;
    public float speedUpScale=1.3f;
    public float speedDownScale=0.7f;
    public float speedUpTime=5;
    public float speedDownTime=5;
    public GameObject[] ballLists=null;
    // Use this for initialization
    void Start () {
       // ballLists = GameObject.FindGameObjectsWithTag("Ball"); //gdyby bylo wiecej kulek i trzeba bylo zrobic cos ze wszystkimi
    }
    void OnTriggerEnter2D(Collider2D coll) 
    {
        Debug.Log("coll.gameObject.tag " + coll.gameObject.tag);
        if (coll.gameObject.tag == "Ball")
        {
            GameObject collGameObject = coll.gameObject;
            if (isSpeedUp) collGameObject.GetComponent<BallScript>().SetSpeedScaleForTime(speedUpTime, speedUpScale);
            else if (isSlowDown) collGameObject.GetComponent<BallScript>().SetSpeedScaleForTime(speedDownTime, speedDownScale);
            else if (isCannonShot)
            {
                List<GameObject> bricksList = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().bricksList;
                GameObject brick = bricksList[(int)Random.Range(0, bricksList.Count)];
                if (brick != null)
                {
                    collGameObject.GetComponent<Rigidbody2D>().velocity = (new Vector2(brick.transform.position.x - collGameObject.transform.position.x, brick.transform.position.y - collGameObject.transform.position.y).normalized * 20);
                }

            }
            Destroy(gameObject);
        }
        else if (coll.gameObject.tag == "Wall") Destroy(gameObject);
    }
     
    // Update is called once per frame
    void FixedUpdate () { 
     
	}
}
