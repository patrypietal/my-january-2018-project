﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour {
    public bool dumbMode = false;
    public float dumbModeWidth = 0;
    public float platformWidth;
    public float ballWidth;
    public float gameScreenWidth;
    public float platformSpeed=0.15f;
    public float platformMaxSpeed = 1;
    public float maxPlatformPositionX;
    public Vector3 platformPosition;


    void Start()
    {
        GameManager gameManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        float adjustPlatformSpeed = platformSpeed / 9 * 16 * gameManagerScript.gameScreenWidth / gameManagerScript.gameScreenHeight;
        Debug.Log("||||||||||||||||||||||||| adjustPlatformSpeed:  " + adjustPlatformSpeed);
        platformSpeed += (adjustPlatformSpeed - platformSpeed) * 0.3f; 


        if (GameObject.FindGameObjectWithTag("GameSettings") != null)
        {
            GameObject GameSettings = GameObject.FindGameObjectWithTag("GameSettings");

            GameSettingsScript settingScript = GameSettings.GetComponent<GameSettingsScript>();

            dumbMode = settingScript.dumbMode;
        }

        if (dumbMode) UpdateDumbBodeWidth();
    }
    private void UpdateDumbBodeWidth()
    {
        dumbModeWidth = ballWidth   + 0.15f;
        UpadePlatformWidth();
    }
    public void UpadePlatformWidth(float w)
    {
        platformWidth = w;
        maxPlatformPositionX = (gameScreenWidth / 2) - dumbModeWidth - (gameObject.transform.localScale.x / 2);
    }
    public void UpadePlatformWidth()
    {
        maxPlatformPositionX = (gameScreenWidth / 2) - dumbModeWidth - (gameObject.transform.localScale.x / 2);
    }
    public void MoveHorizontal(float directionAndSpeed)// -1, 1
    {
        platformPosition = gameObject.transform.position;
        if (directionAndSpeed != 0)
        {
            directionAndSpeed *= platformSpeed;
               directionAndSpeed = Mathf.Clamp(directionAndSpeed, -1, 1);
            platformPosition.x += directionAndSpeed;
            PlatformPositionUpdate();


        }
    }

    public void PlatformPositionUpdate()// -1, 1
    {
                platformPosition.x = Mathf.Clamp(platformPosition.x, -maxPlatformPositionX, maxPlatformPositionX);
        gameObject.transform.position = platformPosition;

    }
void FixedUpdate () {
		
	}
}
