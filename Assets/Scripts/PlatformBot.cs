﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBot : MonoBehaviour
{

    public float gameScreenHeight;
    public float gameScreenWidth;
    public float lastBricksRow;
    public float platformHeight;
    public float howLongLineBotPredicts;//jak długa musi być linia, którą pokona piłka, żeby bot ją przewidział
    public GameObject ball;
    public PlatformMove platformMoveScript;
    public Rigidbody2D ballRigidbody;
    Transform platformPosition;
    Transform ballPosition;
    void Start()
    {
       
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        platformMoveScript = gameObject.GetComponent<PlatformMove>();
        if (gameScreenWidth/gameScreenHeight<12/16)
            howLongLineBotPredicts = gameScreenWidth;//test mode
        else  howLongLineBotPredicts = 0.7f * gameScreenWidth;//test mode
        platformHeight = this.gameObject.transform.localScale.y;
        gameScreenWidth -= 0.2f;// kazda polowa jest odjeta przez polowe szerokosci pilki
    }
    void FixedUpdate()
    {
        platformPosition = gameObject.transform;
        ballPosition = ball.transform;
        float ballFinalPosition = ballPosition.position.x;
        if (!(ballRigidbody.velocity.y > 0 || ballPosition.position.y > lastBricksRow))
        {
            Vector2 ballVelocity = ballRigidbody.velocity;
            float a = ballVelocity.y / ballVelocity.x; // a to wspolczynnik kierunkowy prostej do ktorej mozna sprowadzic ruch piłki.
            float fallingBallLineToMeetPlatform = Mathf.Abs((ballPosition.position.y - platformPosition.position.y - platformHeight) / a); // platform height powinno byc /2 - ballHeight/2 ale oba są takie same więc na jedno wychoodzi
            if (fallingBallLineToMeetPlatform < howLongLineBotPredicts)
            {
                bool willBounceFromWall=false;
                float distanceFromBallToWall;
                bool rightDirection;
                if (ballVelocity.x > 0)
                {
                    rightDirection = true;
                    distanceFromBallToWall = gameScreenWidth / 2 - ballPosition.position.x;
                }
                else
                {
                    rightDirection = false;
                    distanceFromBallToWall = Mathf.Abs(-gameScreenWidth / 2 - ballPosition.position.x);
                }
                
                if (fallingBallLineToMeetPlatform > distanceFromBallToWall  )
                {
                    fallingBallLineToMeetPlatform -= distanceFromBallToWall;
                    int evenBouncesFromWalls = (int)(fallingBallLineToMeetPlatform / gameScreenWidth);
                    float finalPositionOfBallFromWall = Mathf.Abs(fallingBallLineToMeetPlatform % gameScreenWidth);
                    if (evenBouncesFromWalls % 2 == 1) rightDirection = !rightDirection;
                    ballFinalPosition = gameScreenWidth / 2 - finalPositionOfBallFromWall;
                    if (!rightDirection) ballFinalPosition = -ballFinalPosition;
                }
                else
                {  if (rightDirection) ballFinalPosition += fallingBallLineToMeetPlatform;
                   else ballFinalPosition -= fallingBallLineToMeetPlatform;
                }
               
            }
        }

        if (ballFinalPosition > platformPosition.position.x + 0.5f) platformMoveScript.MoveHorizontal(0.5f);
        else if (ballFinalPosition > platformPosition.position.x + 0.1f) platformMoveScript.MoveHorizontal(0.2f);
        else if (ballFinalPosition > platformPosition.position.x) platformMoveScript.MoveHorizontal(0.01f);
        else if (ballFinalPosition < platformPosition.position.x - 0.5f) platformMoveScript.MoveHorizontal(-0.5f);
        else if (ballFinalPosition < platformPosition.position.x - 0.1f) platformMoveScript.MoveHorizontal(-0.2f);
        else if (ballFinalPosition < platformPosition.position.x) platformMoveScript.MoveHorizontal(-0.01f);





    }

}
