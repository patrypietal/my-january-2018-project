﻿using UnityEngine;

public class ForceMove : MonoBehaviour
{
    public float forceSpeed = 15;
    public float forceMaxSpeed = 25;
    public float forceMaxRotation = 55;
    public Vector2 forceVelocity;
    public Vector3 forceRotation;
    void Start()
    {

    }
    public void MoveVertical(float directionAndSpeed)// -1, 1
    {

        forceVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
        float velocityX = forceVelocity.x;
        float verticalSpeed = Mathf.Clamp(directionAndSpeed, -1, 1);
        forceVelocity = new Vector2(velocityX, verticalSpeed * forceSpeed); //plus rozwiązania nadawania prędkości zamiast zmieniania pozycji to: predkość zostaje przy skoku i przy "zbiegnięciu z platformy" minut to droga hamowania przy większych prędkościach
        ForceVerticalPositionUpdate();
    }





    public void MoveHorizontal(float directionAndSpeed)// -1, 1
    {
        forceVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
        float velocityY = forceVelocity.y;
        float horizontalSpeed = Mathf.Clamp(directionAndSpeed, -1, 1);
        forceVelocity = new Vector2(horizontalSpeed * forceSpeed, velocityY); //plus rozwiązania nadawania prędkości zamiast zmieniania pozycji to: predkość zostaje przy skoku i przy "zbiegnięciu z platformy" minut to droga hamowania przy większych prędkościach
        ForceHorizonstalPositionUpdate();


    }
    public void Rotate(float angle)// -1, 1
    {
        Debug.Log("transform.rotation.z: " + transform.eulerAngles.z);
        angle *= 1.5f;
         angle = Mathf.Clamp(angle, -1.5f, 1.5f);
        
        if (!(transform.rotation.z*angle> 0 && transform.eulerAngles.z >= (forceMaxRotation) && transform.eulerAngles.z <= (360-forceMaxRotation)))
          gameObject.transform.Rotate(0, 0, angle);
    }
    public void ForceHorizonstalPositionUpdate()// -1, 1
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = forceVelocity;

    }
    public void ForceVerticalPositionUpdate()// -1, 1
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = forceVelocity;

    }
    void FixedUpdate()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity *= 0.90f;
    }
}
