﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    void FixedUpdate()
    {
        if (Input.GetAxis("Horizontal") != 0) gameObject.GetComponent<ForceMove>().MoveHorizontal(Input.GetAxis("Horizontal"));
        if (Input.GetAxis("Vertical") != 0) gameObject.GetComponent<ForceMove>().MoveVertical(Input.GetAxis("Vertical"));
        if (Input.GetAxis("Rotate") != 0) gameObject.GetComponent<ForceMove>().Rotate(Input.GetAxis("Rotate"));

    }

    void Update()
    {

        if (Input.GetButtonDown("Pause")) gameObject.GetComponent<PauseGame>().pauseGame = !gameObject.GetComponent<PauseGame>().pauseGame;
    }
}
