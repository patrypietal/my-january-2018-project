﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceScript : MonoBehaviour {

    CapsuleCollider2D forceCollider; 
    // Use this for initialization
    void Start () {

        forceCollider = gameObject.GetComponent<CapsuleCollider2D>();
    }
    void OnTriggerStay2D(Collider2D coll)

    {

        if (coll.gameObject.tag == "Ball")
        {

            if (coll.gameObject.GetComponent<Rigidbody2D>().velocity.y>0)

            { 
            coll.gameObject.GetComponent<Rigidbody2D>().velocity += (Vector2)(Quaternion.AngleAxis(gameObject.transform.rotation.eulerAngles.z, Vector3.forward) * Vector3.up * coll.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude/5);
                coll.gameObject.GetComponent<Rigidbody2D>().velocity *= 0.95f;
            }

        }
        }
    

    
	// Update is called once per frame
	void Update () {
		
	}
}
