﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasingFunctions : MonoBehaviour {
    //na podstawie  https://gist.github.com/Chovanec/6799170
    public static float EaseOutElastic (float currentTime, float beginningValue, float changeInValue, float duration)
    {
        float s = 1.70158f;
        float p=0f;
        float a = changeInValue;
        if (currentTime == 0) return beginningValue;
        if (((int)(currentTime / duration)) == 1) return (beginningValue + changeInValue);
        if (p == 0f) p = duration * 0.3f;
        if (a < Mathf.Abs(changeInValue))
        {
            a = changeInValue;
            s = p / 4;
        }
        else s = (p / (2 * Mathf.PI) * Mathf.Asin(changeInValue / a));
        return (a*Mathf.Pow(2,(-10*currentTime))*Mathf.Sin((currentTime*duration-s)*(2*Mathf.PI/p))+changeInValue+beginningValue);
    
    }
}
