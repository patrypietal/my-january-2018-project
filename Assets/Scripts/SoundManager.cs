﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    public AudioSource audioSrc;
    public AudioClip startMenuAudioClip;
    public AudioClip loserAudioClip;
    public AudioClip gameAudioClip;
    public AudioClip winnerAudioClip;
    public float volume = 0.35f;
    private AudioClip activeAudioClip = null;
    void Start()
    {
        DontDestroyOnLoad(this);

        SceneManager.activeSceneChanged += SceneChanged;
    }

    private void SceneChanged(Scene arg0, Scene arg1)
    {
        AudioClip actualClip = returnAudioClip(arg1.name);
        if (actualClip != activeAudioClip)
        {
            activeAudioClip = actualClip;
            audioSrc.clip=actualClip;
        }
    }
    private AudioClip returnAudioClip(string sceneName)
    {

        if (sceneName == "Game") return gameAudioClip;
        else if (sceneName == "Winner") return winnerAudioClip;
        else if (sceneName == "Loser") return loserAudioClip;
        else if (sceneName == "StartMenu") return startMenuAudioClip;
        return gameAudioClip;

    }

}
