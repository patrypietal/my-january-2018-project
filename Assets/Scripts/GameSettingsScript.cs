﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingsScript : MonoBehaviour {
    public int platformRows;
    public bool dumbMode; 
    public int defaultPlatformRows = 5; 
    void Start () {
        dumbMode = false;
        setPlatformRows(defaultPlatformRows);
	}
    public void setPlatformRows(int pRows)
    {
        platformRows = pRows;
    }
    void Awake()
    {//kill on scene end

        DontDestroyOnLoad(transform.gameObject);
    }
}
