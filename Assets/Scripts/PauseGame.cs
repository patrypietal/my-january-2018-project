﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public bool pauseGame;
    private bool isPaused;
    private float timeScale;
    GameObject pauseMenu;
    void Start()
    {
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        pauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused != pauseGame) Pause();
    }
    private void Pause()
    {
        if (pauseGame)
        {
            isPaused = true;
            timeScale = Time.timeScale;
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
        }
        else
        {
            pauseMenu.SetActive(false);
            isPaused = false;
            Time.timeScale = timeScale; 
        }
    }
}
