﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{


    public Toggle dumbModeToggle;
    public Slider platformRowsSlider;
    public GameObject GameSettings;
    public GameObject GameSettingsObject;
    void Start ()
    {
       
        if (GameObject.FindGameObjectWithTag("GameSettings")!=null) GameSettings = GameObject.FindGameObjectWithTag("GameSettings");
        else
        {
            GameSettings =Instantiate(GameSettingsObject, Vector3.zero, Quaternion.identity);
        }
    }
    public void GameStart()
    {
       GameSettingsScript settingScript = GameSettings.GetComponent<GameSettingsScript>();
        Debug.Log("platformRowsSlider "+platformRowsSlider.value);
        settingScript.setPlatformRows((int)platformRowsSlider.value);
        settingScript.dumbMode=dumbModeToggle.isOn;
         SceneManager.LoadScene("Game"); 
    }
  
}
