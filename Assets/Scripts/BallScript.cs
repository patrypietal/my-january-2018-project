﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
public class BallScript : MonoBehaviour
{
    ParticleSystem particleSystem;
    public GameManager gameManagerScript;
    public float minSpeed = 8;
    public float maxSpeed = 18;
    private Rigidbody2D ballRigidbody;
    CapsuleCollider2D ballCollider;
    public int stuckY = 0;
    public int stuckX = 0;
    public float speedScale = 1;
    public float speedScaleDuration = 0;
    public float normalSpeedScale = 1;
    public float gameHeight; 
    private bool ifBigParticles = false;
    private bool ifBigBall = false;
    public float timeToEndBigParticles;
    public float maxTimeToEndBigParticles =10;
    public float particleSizeAfterBouncing=0.1f;
    public float particleSizeBeforeBouncing=0.02f;
    public float timeToEndBigBall;
    public float maxTimeToEndBigBall=5;
    private GameObject bounceSprite;
    public float TimeToEndSlowMo = 0;
    GameObject LastSlowMoGameObject = null;
    GameObject miniCameraObj = null;
    SpriteRenderer backgroundSpriteRendererObj = null;
    private IEnumerator coroutineBackgroundColorFlash = null;
    private IEnumerator coroutineMainCameraShake = null;
    Camera mainCamera;
    // Use this for initialization
    void Start()
    {
     
        GetBackground();
        GetMiniCamera();
        GetMainCamera();
        bounceSprite = transform.Find("BounceSprite").gameObject;
        bounceSprite.SetActive(false);
        particleSystem = GetComponent<ParticleSystem>();
        ballRigidbody = gameObject.GetComponent<Rigidbody2D>();
        StartBouncing();
        gameHeight = gameManagerScript.gameScreenHeight;
    }
    void GetMiniCamera()
    {

        miniCameraObj = transform.GetComponentInChildren<Camera>().gameObject;
        miniCameraObj.GetComponent<MiniCameraFollow>().SetHeightAndWidth(gameManagerScript.gameScreenHeight, gameManagerScript.gameScreenWidth);

        miniCameraObj.SetActive(false);
    }
    void GetMainCamera()
    {
        mainCamera = Camera.main;
    }
    void GetBackground()
    {
        if (GameObject.FindGameObjectWithTag("Background") != null)
            backgroundSpriteRendererObj = GameObject.FindGameObjectWithTag("Background").GetComponent<SpriteRenderer>();
       
    }
    public  void SetSpeedScaleForTime(float time, float newSpeedScale)
    {
        if (speedScale != normalSpeedScale)
        {
            EndSpeedScaleForTime();
        }

        normalSpeedScale = speedScale;
        speedScale = newSpeedScale;
        speedScaleDuration = time;
    }
  public  void EndSpeedScaleForTime()
    {
        speedScale = normalSpeedScale;
    }
    void StartBouncing()
    {

        ballRigidbody.velocity = new Vector2((Random.Range(-5, 5)), 5);


    }
    void HowLongSinceBounced()
    {
        if (ifBigParticles)
        {
            if (timeToEndBigParticles <= 0)
            {
                var main = particleSystem.main;
                main.startSize = particleSizeBeforeBouncing;
                ifBigParticles = false;
            }
            else timeToEndBigParticles -= Time.deltaTime;
        }
        if (ifBigBall)
        {
            if (timeToEndBigBall <= 0)
            {

                bounceSprite.SetActive(false);
                ifBigBall = false;
            }
            else timeToEndBigBall -= Time.unscaledDeltaTime;
        }
    }

    void Bounced()
    {
        if (coroutineBackgroundColorFlash!=null)    StopCoroutine(coroutineBackgroundColorFlash);
        coroutineBackgroundColorFlash = BackgroundColorFlash(1);
        StartCoroutine(coroutineBackgroundColorFlash);

        coroutineMainCameraShake = MainCameraShake(1);
        StartCoroutine(coroutineMainCameraShake);


        if (timeToEndBigParticles <= 0)
            {
                var main = particleSystem.main;
            main.startSize = particleSizeAfterBouncing;
            ifBigParticles = true;
        }
        if (timeToEndBigBall <= 0)
        {

            bounceSprite.SetActive(true);
            ifBigBall = true;
        }
        timeToEndBigParticles = maxTimeToEndBigParticles;
        timeToEndBigBall = maxTimeToEndBigBall;


    }
    void OnCollisionEnter2D(Collision2D coll)
    {

            Bounced();
        if (coll.gameObject.tag == "Brick")
        {
            gameManagerScript.bricksList.Remove(coll.gameObject);
            Destroy(coll.gameObject);
        
        }
        

    }
    void SlowMoBeforeHit()
    {
         
        float distanceToHit = 2.5f;
        float secondDistanceToHit = 1f;
        float slowMoTimeScale = 0.4f;
        float slowMoDuration = 0.30f;
        float secondSlowMoDuration = 0.14f;
        float secondSlowMoTimeScale = 0.15f;
        TimeToEndSlowMo -= Time.unscaledDeltaTime;
        EndSlowMo();
        var layerMask = (1 << LayerMask.NameToLayer("Force"));
        //        layerMask |= (1 << name);
        layerMask = ~layerMask;
        RaycastHit2D hit = Physics2D.Raycast(this.gameObject.transform.position, ballRigidbody.velocity, distanceToHit, layerMask);

        //If something was hit.
        if (hit.collider != null)
        {
            Debug.Log(" hit.distance: " + hit.distance);
            Debug.Log(" hit.collider: " + hit.collider);

            /*  if (hit.collider.gameObject != LastSlowMoGameObject)
              {
                  if (hit.collider.gameObject.tag == "Brick")
                  {
                      LastSlowMoGameObject = hit.collider.gameObject;
                      StartSlowMo(slowMoDuration, slowMoTimeScale);
                  }
                  else if (hit.collider.gameObject.tag == "Bonuses" && hit.distance<secondDistanceToHit )
                  {
                      if (hit.collider.gameObject.name=="CannonShot")
                      { 
                      LastSlowMoGameObject = hit.collider.gameObject;
                      StartSlowMo(secondSlowMoDuration, secondSlowMoTimeScale);
                      }
                  }

              }
              else
            {
                if (hit.collider.gameObject.tag == "Brick" && hit.distance < secondDistanceToHit)
                {
                    LastSlowMoGameObject = hit.collider.gameObject;
                    StartSlowMo(secondSlowMoDuration, secondSlowMoTimeScale);
                }
            }*/
            if (hit.distance < secondDistanceToHit)
            {
                if (hit.collider.gameObject.tag == "Brick" )
                {
                    StartSlowMo(secondSlowMoDuration, secondSlowMoTimeScale);
                }
                else if (hit.collider.gameObject.tag == "Bonuses" )
                {
                    if (hit.collider.gameObject.name == "CannonShot(Clone)")
                    {
                        StartSlowMo(secondSlowMoDuration, secondSlowMoTimeScale);
                    }
                }
            }
        }


    }
    void StartSlowMo(float SlowMoDuration, float SlowMoTimeScale)
    {
        TimeToEndSlowMo = SlowMoDuration;
        Time.timeScale = SlowMoTimeScale;
        miniCameraObj.SetActive(true);
    }
    void EndSlowMo()
    {
       if (TimeToEndSlowMo<=0)
        {
            Time.timeScale = 1;
            miniCameraObj.SetActive(false);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        SlowMoBeforeHit();
        HowLongSinceBounced();
        if (gameObject.transform.position.y< -gameHeight/2)
        {

            SceneManager.LoadScene("Winner");
        }
        if (speedScale != normalSpeedScale)
        {
            speedScaleDuration -= Time.deltaTime;
            if (speedScaleDuration < 0) EndSpeedScaleForTime();

        }

        if (Mathf.Abs(ballRigidbody.velocity.y) < 0.5f)
        {
            stuckY++;
            if (stuckY > 25)
            {
                Debug.Log("stuckfix");
                stuckY = 0;
                if (ballRigidbody.velocity.y > 0) ballRigidbody.velocity += new Vector2(0, 0.2f);
                else  ballRigidbody.velocity -= new Vector2(0, 0.2f);
            }
        }
        else
        {
            stuckY = 0;
        }
        if (Mathf.Abs( ballRigidbody.velocity.x )< 0.5f)
        {
            Debug.Log("ballRigidbody.velocity.x: "+ ballRigidbody.velocity.x);
            stuckX++;
            if (stuckX > 30)
            {
                Debug.Log("stuckfix");
                stuckX = 0;
                if (ballRigidbody.velocity.x > 0) ballRigidbody.velocity += new Vector2(0.2f, 0);
                else if (ballRigidbody.velocity.x < 0) ballRigidbody.velocity -= new Vector2(0.2f, 0);
                else if (ballRigidbody.gameObject.transform.position.x > 0) ballRigidbody.velocity -= new Vector2(0.2f, 0);
                else  ballRigidbody.velocity += new Vector2(0.2f, 0);
            }
        }
        else
        {
            stuckX = 0;
        }
        float velocity = ballRigidbody.velocity.magnitude;
        if (velocity == 0) ballRigidbody.velocity = new Vector2((Random.Range(-1, 1)),(Random.Range(-1, 1)));
        else if (velocity < getMinSpeed()) ballRigidbody.velocity *= 1.05f;
        else if (velocity > getMaxSpeed()) ballRigidbody.velocity *= 0.95f;
        else if (velocity != getMinSpeed()) ballRigidbody.velocity *= 0.99f;

    }
    private float getMaxSpeed()
    {
        return maxSpeed * speedScale;
    }
    private float getMinSpeed()
    {
        return minSpeed * speedScale;
    }

    IEnumerator BackgroundColorFlash(float duration)
    {

        Color backgroundColor = backgroundSpriteRendererObj.color;
        Debug.Log("  backgroundColor.a :" + backgroundColor.a);
        float beginningValue = 0.3f;
        float changeInValue = 1 - beginningValue;
        float currentTime = 0;
        float newAlpha;
        while (currentTime < 1)
        {
            yield return null;
            currentTime += Time.deltaTime / duration;
            newAlpha = EasingFunctions.EaseOutElastic(currentTime, beginningValue, changeInValue, 1);

            backgroundColor.a = Mathf.Clamp(newAlpha, 0f, 1f);
            backgroundSpriteRendererObj.color = backgroundColor;
        }

    }
    IEnumerator MainCameraShake(float duration)
    {
        float shakeX = Random.Range(-0.5f, 0.5f);
        float shakeY = Random.Range(-0.5f, 0.5f);
        float lastShakeX = 0f;
        float lastShakeY = 0f;
        duration /= 2;
        float currentTime = 0;
        float newShakeX;
        float newShakeY;
        while (currentTime < 0.2f)
        {
            yield return null;
            currentTime += Time.deltaTime / duration;
            newShakeX = EasingFunctions.EaseOutElastic(currentTime, 0, shakeX, 1);
            newShakeY = EasingFunctions.EaseOutElastic(currentTime, 0, shakeY, 1);
            mainCamera.transform.position += new Vector3(newShakeX - lastShakeX, newShakeY - lastShakeY, 0);

            lastShakeX = newShakeX;
            lastShakeY = newShakeY;


        }
          currentTime = 0;
        while (currentTime < 1)
        {
            yield return null;
            currentTime += Time.deltaTime / duration;
            newShakeX = EasingFunctions.EaseOutElastic(currentTime, shakeX, -shakeX, 1);
            newShakeY = EasingFunctions.EaseOutElastic(currentTime, shakeY, -shakeY, 1);
            mainCamera.transform.position += new Vector3(newShakeX - lastShakeX, newShakeY - lastShakeY, 0);

            lastShakeX = newShakeX;
            lastShakeY = newShakeY;


        }

    }

    IEnumerator ObjectAnimationEasingPositionX(GameObject obj, float duration, float delayBeforeStart, float changeInValue)
    {
        float beginningValue = obj.transform.position.x - changeInValue;
        obj.transform.position = new Vector3(beginningValue, obj.transform.position.y, obj.transform.position.z);

        float newXPos;
        if (delayBeforeStart > 0) yield return new WaitForSeconds(delayBeforeStart);
        float currentTime = 0;
        while (currentTime < 1)
        {
            yield return null;
            currentTime += Time.deltaTime / duration;
            newXPos = EasingFunctions.EaseOutElastic(currentTime, beginningValue, changeInValue, 1);
            obj.transform.position = new Vector3(newXPos, obj.transform.position.y, obj.transform.position.z);


        }

    }
}

