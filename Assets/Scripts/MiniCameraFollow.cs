﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniCameraFollow : MonoBehaviour
{
    GameObject parentObj;
    Camera thisCamera;  
    float gameScreenHeight = 10;
    float gameScreenWidth = 5;
    // Use this for initialization
    void Start () {
        thisCamera = this.gameObject.GetComponent<Camera>();
        parentObj = this.transform.parent.gameObject;
    }
    public void SetHeightAndWidth(float newGameScreenHeight, float newGameScreenWidth)
    {

         gameScreenHeight = newGameScreenHeight;
         gameScreenWidth = newGameScreenWidth;
    }

    // Update is called once per frame
    void LateUpdate ()
    {
        Vector3 parentPos = parentObj.transform.position;
        float w;
        float h =0.35f;
        if (gameScreenWidth / gameScreenHeight < 0.6f) w = 2 * h;
        else if (gameScreenWidth / gameScreenHeight < 0.75f) w = 1.5f * h;
        else if(gameScreenWidth / gameScreenHeight < 0.9f) w = 1.2f * h;
        else w = h;
        float x = Mathf.Clamp((((parentPos.x+ (gameScreenWidth/2)) / gameScreenWidth)-(w/2)), 0, 1-w);

        float y =Mathf.Clamp((((parentPos.y + (gameScreenHeight / 2)) / gameScreenHeight)-(h/2)), 0,1-h);
        if (y > 0.5f) y -= h * 0.75f;
        else   y += h*0.75f;
        Rect newRect = new Rect(x,y,w,h);
        thisCamera.rect = newRect;

    }
}
