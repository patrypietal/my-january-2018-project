﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour
{
    public GameObject[] bonuses;
    public int minDelay = 1;
    public int maxDelay = 5;
    private float delay;
    public float GameWidth;
    public float GameHeight;
    // Use this for initialization
    void Start () {

        delay = Random.Range(minDelay,maxDelay);
        GameManager scriptGameManager = gameObject.GetComponent<GameManager>();
        GameWidth = scriptGameManager.gameScreenWidth;
        GameHeight = scriptGameManager.gameScreenHeight;
    } 
    void Update () {
        delay -= Time.deltaTime;
        if (delay<0)
        {//srednio co drugie pojawienie to wyrzutnia
            int randomNumber = Mathf.Clamp((int)Random.Range(0, 5), 0, 2);
            Debug.Log(randomNumber);
            Instantiate(bonuses[randomNumber], new Vector3(Random.Range(-GameWidth/2 + 1, GameWidth/2 - 1), GameHeight/2 - 1, 1), Quaternion.identity); 
            delay = Random.Range(minDelay, maxDelay);
        }
	}
}
