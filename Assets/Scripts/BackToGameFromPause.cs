﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToGameFromPause : MonoBehaviour {

	// Use this for initialization
	
    public void BackToGame ()
    {

       GameObject.FindGameObjectWithTag("Player").GetComponent<PauseGame>().pauseGame = false; 
    }
}
